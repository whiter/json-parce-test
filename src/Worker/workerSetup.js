export default class WebWorker {
    postMessage(arg0) {
        throw new Error("Method not implemented.");
    }
    addEventListener(arg0, arg1) {
        throw new Error("Method not implemented.");
    }
	constructor(worker) {
		const code = worker.toString();
		const blob = new Blob(['('+code+')()']);
		return new Worker(URL.createObjectURL(blob));
	}
}
