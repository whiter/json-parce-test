import * as React from "react";
import { getData, getDataLikeABoss, getJsonStream } from "../api";
import worker from "../Worker/worker.js";
import WebWorker from "../Worker/workerSetup";
const parceWorker = new WebWorker(worker);

const FILE = "/data100k.json";

type State = {
  value: number;
  running: boolean;
  intervalId?: number;
  dataRecords?: number;
  dataLikeABossRecords?: number;
  status?: string;
  likeABossStatus?: string;
  itemsCount: number;
};

type Props = {};
class Timer extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { value: 0, running: true, itemsCount: 0 };
  }

  componentDidMount() {
    const intervalId = window.setInterval(this.tick, 250);
    this.setState({ intervalId });
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  render() {
    return (
      <>
        <div style={{ fontSize: 200 }}>{this.state.value}</div>
        <div>
          <button onClick={this.handleToggle}>
            {this.state.running ? "Pause" : "Go"}
          </button>
        </div>

        <div>{this.state.status}</div>
        <div>
          Number of records: {this.state.dataRecords || 0}
          <button onClick={this.handleGetData}>Get data</button>
        </div>
        <div>
          <div>{this.state.likeABossStatus}</div>
          Number of records: {this.state.dataLikeABossRecords || 0}
          <button onClick={this.handleGetDataLikeABoss}>
            Get data Like a Boss
          </button>
        </div>
        <div>
          <div>{this.state.likeABossStatus}</div>
          Number of records: {this.state.itemsCount || 0}
          <button onClick={this.handleGetDataAsStream}>
            Get data as stream
          </button>
        </div>
        {/* {this.renderItems()} */}
      </>
    );
  }

  renderItems = () => {
    const { itemsCount } = this.state;
    return <div>{itemsCount} objects loaded</div>;
  };

  handleToggle = () => {
    this.setState(state => ({ running: !state.running }));
  };

  handleGetData = () => {
    this.setState({ status: "Downloading" });
    getData(FILE).then(res => {
      this.setState({ status: "Transforming" });
      console.log("Transforming:", res);
      this.setState({ dataRecords: res.data.length, status: "Done" });
    });
  };

  handleGetDataLikeABoss = () => {
    this.setState({ likeABossStatus: "Downloading" });
    getDataLikeABoss(FILE).then(res => {
      this.setState({
        dataLikeABossRecords: res.data.length,
        likeABossStatus: "Done"
      });
    });
  };

  handleGetDataAsStream = () => {
    getJsonStream(FILE, "{email}", newItems => {
      this.setState(state => ({
        itemsCount: state.itemsCount + newItems.length
      }));
    });
  };

  tick = () => {
    if (this.state.running) {
      this.setState(state => ({ value: state.value + 1 }));
    }
  };
}

export default Timer;
