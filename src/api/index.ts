import axios from "axios";
import worker from "../Worker/worker.js";
import WebWorker from "../Worker/workerSetup";
import oboe from "oboe";

const parceWorker = new WebWorker(worker);

const rawAxios = axios.create();

axios.interceptors.response.use(
  function(response) {
    console.log(typeof response.data);
    return new Promise((resolve: any) => {
      parceWorker.postMessage(response.data);
      parceWorker.addEventListener("message", (event: any) => {
        console.log("event recived");
        resolve({ ...response, data: event.data });
      });
    });
  },
  function(error) {
    // Do something with response error
    return Promise.reject(error);
  }
);

export const getData = (url: string): Promise<any> => {
  console.log("downloading");
  return rawAxios.get(url, {
    transformResponse: [
      function(data, headers) {
        console.log("transforming");
        return JSON.parse(data);
      }
    ]
  });
};

export const getDataLikeABoss = (url: string): Promise<any> => {
  console.log("downloading");
  return axios.get(url, {
    transformResponse: [
      function(data) {
        return data;
      }
    ]
  });
};

function webWorkerParce(data: string): Promise<any> {
  return new Promise((resolve: any) => {
    parceWorker.postMessage(data);
    parceWorker.addEventListener("message", (event: any) => {
      console.log("event recived");
      resolve(event.data);
    });
  });
}

export const getJsonStream = (
  url: string,
  duckType: string,
  cb?: (obj: any[]) => void
) => {
  let count = 0;
  let buffer: any = []
  oboe(url)
    .node(duckType, function(obj) {
      count++;
      buffer.push(obj.email);
      if (count % 1000 === 0) {
        console.log(count);
        if (cb) {
            cb(buffer);
        }
        buffer = [];
      }
      return oboe.drop;
    })
    .done(function(finalJson) {
        if (cb) {
            cb(buffer);
          }
      console.log(finalJson); // logs: {"drinks":[]}
    });
};
