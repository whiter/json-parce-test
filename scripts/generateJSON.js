const faker = require('faker');
const fs = require('fs');

const RECORDS_NUMBER = 25000;

const records = new Array(RECORDS_NUMBER).fill(null).map((_, i)=>{
    if (i % 1000===0) {
        console.log(`${i/RECORDS_NUMBER*100}% complete`)
    }
    return faker.helpers.createCard()
})

json = JSON.stringify(records); //convert it back to json
fs.writeFile('./public/data.json', json, 'utf8', (err) => {
    if (err) {
        console.log(err)
    } else {
        console.log("Complete")
    }

});